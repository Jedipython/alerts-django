import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_alerts.settings')

app = Celery('django_alerts')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


@app.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    """
    Запускает периодические задачи с интервалом в указанное количество секунд.
    """
    sender.add_periodic_task(60.0, tasks.s(), name='check_for_tasks_run')


@app.task
def tasks():
    """
    Временная функция-задача, для запуска в setup_periodic_tasks.
    В будущем перенести ее в setup_periodic_tasks.
    """
    from alerts.tasks import check_for_tasks
    check_for_tasks()
