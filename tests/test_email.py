from django.contrib.auth.models import User
from django.core import mail
from django.test import Client, TestCase


class EmailTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = self.client.post('/auth/signup/',
                                     {'username': 'FirstUser',
                                      'password1': '94zlbC7ha6FJ',
                                      'password2': '94zlbC7ha6FJ',
                                      'email': 'myseoru@gmail.com'})
        self.user = User.objects.get(username="FirstUser")
        self.client.force_login(self.user)

    def test_mail(self):
        """
        Автоотправка письма произошла после регистрации юзера.
        Проверяем, что письмо лежит в исходящих.
        """
        self.assertEqual(len(mail.outbox), 1)

    def test_Mail_Subjects(self):
        """Проверяем, что тема первого письма правильная."""
        self.assertEqual(mail.outbox[0].subject, 'Подтверждение регистрации')
