from django.test import TestCase


class PagesTest(TestCase):

    def test_profile(self):
        response = self.client.get('/FirstUser/')
        self.assertAlmostEqual(response.status_code, 200)

    def test_account(self):
        response = self.client.get('/account/')
        self.assertAlmostEqual(response.status_code, 200)

    def test_404(self):
        response = self.client.get('/ffsdfsfwefwefsc/')
        self.assertEqual(response.status_code, 404)
