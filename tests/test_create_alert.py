import datetime

from django import forms
from django.contrib.auth import get_user_model
from django.test import Client, TestCase

from alerts.models import AlertTask

User = get_user_model()


class NewAlertTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = self.client.post('/auth/signup/',
                                     {'username': 'FirstUser',
                                      'password1': '94zlbC7ha6FJ',
                                      'password2': '94zlbC7ha6FJ',
                                      'email': 'myseoru@gmail.com'})
        self.user = User.objects.get(username="FirstUser")
        self.client.force_login(self.user)
        self.client.login(username='FirstUser', password='94zlbC7ha6FJ')

    def test_new_alert_get(self):

        try:
            response = self.client.get('/alerts/create/')
        except Exception as err:
            assert False, f'''
            Страница `/alerts/create/' не работает. Ошибка: `{err}`
            '''
        assert response.status_code != 404, \
            'Страница `/alerts/create/` не найдена, \
            проверьте этот адрес в urls.py'
        assert 'form' in response.context, \
            'Проверьте, что передали форму `form` в \
            контекст страницы `/alerts/create/`'
        assert len(response.context[
                       'form'].fields) == 5, \
            'Проверьте, что в форме `form` на `/alerts/create/` 5 полей'

        assert 'header' in response.context['form'].fields, \
            'Проверьте, что в форме `form` на `/alerts/create/` \
            есть поле `header`'
        assert type(response.context['form'].fields[
                        'header']) == forms.models.CharField, \
            'Проверьте, что в форме `form` на `/alerts/create/` \
            поле `header` типа `CharField`'
        assert response.context['form'].fields['header'].required, \
            'Проверьте, что в форме `form` на странице \
            `/alerts/create/` поле `header`  обязательно'

        assert 'description' in response.context['form'].fields, \
            'Проверьте, что в форме `form` на странице `/alerts/create/` \
            есть поле `description`'
        assert type(
            response.context['form'].fields['description']
        ) == forms.fields.CharField, \
            'Проверьте, что в форме `form` на странице `/alerts/create/` \
            поле `description` типа `CharField`'
        assert response.context['form'].fields['description'].required, \
            'Проверьте, что в форме `form` на странице `/alerts/create/` \
            поле `description` обязательно'

        assert 'location' in response.context['form'].fields, \
            'Проверьте, что в форме `form` на странице `/alerts/create/` \
            есть поле `location`'
        assert type(response.context['form'].fields[
                        'location']) == forms.fields.CharField, \
            'Проверьте, что в форме `form` на странице `/alerts/create/` \
            поле `location` типа `CharField`'

        assert 'date_expire' in response.context['form'].fields, \
            'Проверьте, что в форме `form` на странице `/alerts/create/` \
            есть поле `date_expire`'
        assert type(response.context['form'].fields[
                        'date_expire']) == forms.fields.DateTimeField, \
            'Проверьте, что в форме `form` на странице `/alerts/create/` \
            поле `date_expire` типа `DateTimeField`'

        assert 'participants' in response.context['form'].fields, \
            'Проверьте, что в форме `form` на странице `/alerts/create/` \
            есть поле `participants`'
        assert type(response.context['form'].
                    fields['participants']) == forms.fields. \
                   ModelMultipleChoiceField, \
            'Проверьте, что в форме `form` на странице `/alerts/create/` \
            поле `participants` типа `ModelMultipleChoiceField`'

    def test_new_alert_post(self):
        try:
            response = self.client.get('/alerts/create/')
        except Exception as err:
            assert False, f'''
            Страница `/alerts/create/` не работает. Ошибка: `{err}`
            '''
        url = '/alerts/create/' if response.status_code in (301, 302) \
            else '/alerts/create'
        header = "Тестовое напоминание"
        description = "Описание напоминания"
        location = "Минск"
        date_expire = datetime.datetime.now() + datetime.timedelta(minutes=1)
        author = self.user

        response = self.client.post(url, data={
            'header': header,
            'description': description,
            'location': location,
            'date_expire': date_expire,
            'author': author,
        })

        assert response.status_code in (301, 302), \
            'Проверьте, что со страницы `/alerts/create/` после создания \
            оповещения перенаправляете на страницу аккаунта'
        assert self.post is not None, 'Проверьте, что вы сохранили оповещение \
         при отправки формы на странице `/alerts/create/`'
        assert response.url == '/account/', 'Проверьте, что перенаправляете \
         на главную страницу `/alerts/create/`'
        self.assertEqual((
            AlertTask.objects.filter(author=self.user).count()), 1)

        header = 'Проверка нового оповещения номер 2!'
        response = self.client.post(url, data={
            'header': header,
            'description': description,
            'location': location,
            'date_expire': date_expire,
            'author': author,
        })

        assert response.status_code in (301, 302), \
            'Проверьте, что со страницы `/alerts/create/` после создания \
            оповещения перенаправляете на страницу аккаунта'
        assert self.post is not None, 'Проверьте, что вы сохранили оповещение \
          при отправки формы на странице `/alerts/create/`'
        assert response.url == '/account/', 'Проверьте, что перенаправляете \
          на главную страницу `/alerts/create/`'

        response = self.client.post(url)
        assert response.status_code == 200, \
            'Проверьте, что на странице `/alerts/create/` \
            выводите ошибки при неправильной заполненной формы `form`'
        self.assertEqual((
            AlertTask.objects.filter(author=self.user).count()), 2)

    def test_new_alert_logout(self):
        self.client.logout()
        response = self.client.get('/alerts/create/')
        self.assertAlmostEqual(response.status_code,
                               302)
