from django.contrib.auth.models import User
from rest_framework import serializers

from alerts.models import AlertTask


class RegisterSerializer(serializers.ModelSerializer):
    """Сериализатор данных при регистрации пользователя."""
    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'first_name', 'last_name')
        extra_kwargs = {
            'password': {'write_only': True},
        }

    def create(self, validated_data):
        user = User.objects.create_user(
            validated_data['username'],
            password=validated_data['password'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        return user


class UserSerializer(serializers.ModelSerializer):
    """Список зарегистрированных пользователей."""
    class Meta:
        model = User
        fields = '__all__'


class AlertSerializer(serializers.ModelSerializer):
    """Список оповещений пользователя."""
    class Meta:
        model = AlertTask
        fields = '__all__'
