from django.core.mail import send_mail
from django.views.generic import CreateView

from .forms import CreationForm


class SignUp(CreateView):
    """Представление страницы регистрации."""
    form_class = CreationForm
    success_url = "/auth/login/"
    template_name = "registration/signup.html"

    def form_valid(self, form):
        email = form.cleaned_data['email']
        send_mail_ls(email)
        return super().form_valid(form)


def send_mail_ls(email):
    """ПОтправляет письмо на емейл пользователя."""
    send_mail('Подтверждение регистрации', 'Вы зарегистрированы!',
              'django_alerts <admin@django_alerts.ru>',
              [email], fail_silently=False)
