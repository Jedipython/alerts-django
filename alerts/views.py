from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic import View

from .forms import AlertTaskForm
from .mixins import ObjectMixin
from .models import AlertTask, Participant
from .utils import get_partisipants_list, get_users


class Index(ObjectMixin, View):
    """Представление главной страницы."""
    template = 'index.html'


class Profile(ObjectMixin, View):
    """Представление страницы профиля пользователя."""
    template = 'profile.html'


class Account(LoginRequiredMixin, ObjectMixin, View):
    """Представление страницы личного кабинета."""
    template = 'account.html'


def get(request):
    form = AlertTaskForm()
    return render(request, 'form_alert.html', context={'form': form})


class AlertCreate(LoginRequiredMixin, View):
    """Представление страницы создания напоминания."""

    def get(self, request):
        form = AlertTaskForm()
        return render(request, 'form_alert.html', context={'form': form})

    def post(self, request):
        form = AlertTaskForm(request.POST)
        if form.is_valid():
            new = form.save(commit=False)
            new.author_id = request.user.id
            new.save()

            users_list = get_users(request, request.user.id)
            print(users_list, type(users_list))
            participant_list = [Participant(
                participant=user, task=new) for user in users_list]
            Participant.objects.bulk_create(participant_list)

            return redirect('alerts:account_url')

        return redirect('alerts:account_url')


class AlertsPage(LoginRequiredMixin, View):
    """Представление для страницы с напоминаниями в личном кабинете."""

    def get(self, request, id=None):
        if id is None:
            my_alerts = AlertTask.objects.filter(author=request.user)
            try:
                invited_me_alerts = AlertTask.objects.filter(
                    participant_task__participant=request.user).\
                    exclude(author=request.user)

                return render(request, 'alerts.html', context={
                    'my_alerts': my_alerts,
                    "invited_me_alerts": invited_me_alerts,
                })
            except Exception:

                return render(request, 'alerts.html', context={
                    'my_alerts': my_alerts, })
        else:
            alert = get_object_or_404(AlertTask, id=id)
            participants = Participant.objects.filter(task=id)
            user_active_status = get_object_or_404(
                Participant, participant=request.user, task=id)

            return render(request, 'single_alert.html', context={
                'alert': alert, 'participants': participants,
                'user_active_status': user_active_status,
            })


class EditAlert(LoginRequiredMixin, View):
    """Представление формы редактирования напоминания."""

    def get(self, request, id):
        alert = get_object_or_404(AlertTask, author=request.user, id=id)
        if request.user != alert.author:
            return redirect('alerts:alerts_page_url')

        form = AlertTaskForm(instance=alert)
        participants_list = get_partisipants_list(id)
        return render(request, 'form_alert.html', context={
            'form': form,
            'alert': alert,
            'participants_list': participants_list,
        })

    def post(self, request, id):
        alert = get_object_or_404(AlertTask, author=request.user, id=id)
        if request.user != alert.author:
            return redirect('alerts:alerts_page_url')

        form = AlertTaskForm(request.POST, instance=alert)

        if form.is_valid():
            users_list = get_users(request)
            participant_list = [user for user in users_list]
            for user in participant_list:
                Participant.objects.update_or_create(
                    participant=user, task=alert)
            form.save()
        else:
            return render(request, 'form_alert.html', context={
                'form': form,
                'alert': alert
            })

        return redirect('alerts:alerts_page_url')


class DeleteAlert(LoginRequiredMixin, View):
    """Представление формы удаления напоминания."""

    def post(self, request, pk):
        alert = get_object_or_404(AlertTask, id=pk)
        if request.method == 'POST' and request.user == alert.author:
            alert.delete()
        else:
            return redirect('alerts:alerts_page_url')
        return render(request, 'account.html')


def change_task_status(request, id):
    """
    Изменяет статус конкретного оповещения через кнопку на странице оповещения.
    Простое решение без JS.
    """

    status = get_object_or_404(Participant, task=id, participant=request.user)
    if request.GET.get('status_button'):
        status.task_active = False
    else:
        status.task_active = True
    return render(request, 'account.html')
