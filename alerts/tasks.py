import datetime
import logging

from celery import shared_task
from django.core.mail import send_mail
from django.utils import timezone

from .models import AlertTask, User


@shared_task
def check_for_tasks() -> None:
    """
    Выполняет поиск AlertTask, у которых истек date_expire,
    вытаскивает пользователей с этим AlertTask и task_active=True,
    и обращается к рассылке писем для них.
    """

    tasks_ended = AlertTask.objects.filter(
        date_expire__lt=datetime.datetime.now(tz=timezone.utc))
    for task in tasks_ended:
        users = User.objects.filter(participant_participant__task=task,
                                    participant_participant__task_active=True)
        email_list = [user.email for user in users]
        for email in email_list:
            task_params = [
                task.author,
                task.header,
                task.description,
                task.location,
                task.date_expire]
            try:
                send_mail_ls(email, task_params)
            except Exception as err:
                logging.error(err)


def send_mail_ls(email: str, task_params: list) -> None:
    """
    Функция принимает str email, на которые нужно отослать письмо и параметры,
    которые в них нужно использовать. И выполняет рассылку.
    """

    send_mail(f'Напоминание о событии {task_params[1]} от {task_params[0]}.',
              f'Напоминание о событии {task_params[1]} от {task_params[0]}. \
              Место: {task_params[3]}, дата {task_params[4]}, \
              описание: {task_params[2]}',
              'django_alerts <admin@django_alerts.ru>', [email],
              fail_silently=False
              )
