from django.shortcuts import get_object_or_404, render

from .models import User


class ObjectMixin:
    """
    Custom Mixin для предовтарщения дублирования кода.
    Рендерит страницы в зависимости от переданных template.
    """
    model = User
    template = None

    def get(self, request, username=None):
        if username:
            obj = get_object_or_404(User, username=username)
        else:
            obj = None

        return render(request, self.template, {'obj': obj})
