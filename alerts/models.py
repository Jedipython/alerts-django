from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class AlertTask(models.Model):
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="user")
    header = models.CharField(max_length=150)
    description = models.CharField(max_length=150)
    location = models.CharField(max_length=150)
    date_created = models.DateTimeField(auto_now=True)
    date_expire = models.DateTimeField()

    def __str__(self):
        return self.header


class Participant(models.Model):
    participant = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="participant_participant")
    task = models.ForeignKey(AlertTask, on_delete=models.CASCADE,
                             related_name="participant_task")
    task_active = models.BooleanField(default=True)
