from django.urls import path

from . import views

app_name = 'alerts'

urlpatterns = [
    path("", views.Index.as_view(),
         name="index_url"),
    path("account/", views.Account.as_view(),
         name="account_url"),
    path("alerts/create/", views.AlertCreate.as_view(),
         name="alert_create"),
    path("alerts/<int:id>/", views.AlertsPage.as_view(),
         name="alert_page_url"),
    path("alerts/<int:id>/edit/", views.EditAlert.as_view(),
         name="alert_edit_url"),
    path("alerts/<int:pk>/delete/", views.DeleteAlert.as_view(),
         name="alert_delete_url"),
    path("alerts/", views.AlertsPage.as_view(),
         name="alerts_page_url"),
    path('alerts/<int:id>/active/', views.change_task_status,
         name="alerts_status_url"),
    path("<username>/", views.Profile.as_view(),
         name="profile_url"),
]
