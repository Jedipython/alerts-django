from django import forms
from django.forms.widgets import SelectDateWidget

from .models import AlertTask, User


class AlertTaskForm(forms.ModelForm):

    participants = forms.ModelMultipleChoiceField(
        queryset=User.objects.all(),
        required=False,
        widget=forms.CheckboxSelectMultiple, )

    class Meta:
        model = AlertTask
        fields = ('header', 'description', 'location', 'date_expire',)
        widgets = {
            "date_expire": SelectDateWidget(),
            'description': forms.Textarea(attrs={
                'class': 'form-control', 'rows': 6
            }),
        }
