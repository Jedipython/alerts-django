from django.db.models.query import QuerySet

from .models import Participant, User


def get_users(request, author_id=None) -> QuerySet:
    """
    Забираеет прикрепленных пользователей из request.
    """
    get_users_from_request = request.POST.getlist('participants')
    if author_id is not None:
        get_users_from_request.append(author_id)
    return User.objects.filter(id__in=get_users_from_request)


def get_partisipants_list(alert_id: int) -> QuerySet:
    """Возвращает список прикрепленных пользователей в виде QuerySet."""
    return Participant.objects.filter(task=alert_id)
